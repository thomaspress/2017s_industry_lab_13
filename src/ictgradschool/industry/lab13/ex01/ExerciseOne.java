package ictgradschool.industry.lab13.ex01;

import ictgradschool.industry.lab13.examples.example02b.ThreadSafeTheatreSeat;

/**
 * Created by tpre939 on 12/12/2017.
 */
public class ExerciseOne {



    public static void main(String[] args){
//        ExerciseOne runner = new ExerciseOne();
        Thread myThread = new Thread(new Runnable(){
            public void run(){
                //remember the while loop is not checking condition again until for loop is finished!
                while (!Thread.currentThread().isInterrupted()) {
                    for (int i = 0; i < 1000000; i++) {
                        System.out.println(i);
                    }
                }
                System.out.println("Interrupt received");
            }
        });
        myThread.start();
        System.out.println("Interrupting");
            myThread.interrupt();



    }

}
